﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace ModuleDatabase
{
    public class ModuleDatabaseModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}