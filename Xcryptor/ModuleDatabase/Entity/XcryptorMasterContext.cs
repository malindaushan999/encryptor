﻿using System;
using System.Configuration;
using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Npgsql;

#nullable disable

namespace ModuleDatabase.Entity
{
    public partial class XcryptorMasterContext : DbContext
    {
        public XcryptorMasterContext()
        {
            GetConnectionString();
        }

        public XcryptorMasterContext(DbContextOptions<XcryptorMasterContext> options)
            : base(options)
        {
            GetConnectionString();
        }

        public virtual DbSet<m_hash> m_hash { get; set; }
        public virtual DbSet<m_main> m_main { get; set; }
        public virtual DbSet<m_project> m_project { get; set; }

        private static NpgsqlConnection GetConnectionString()
        {
            try
            {
                var filename = Assembly.GetExecutingAssembly().Location;
                var configuration = ConfigurationManager.OpenExeConfiguration(filename);
                var encodedPwd = configuration.AppSettings.Settings["DBPassword"].Value;
                string decodedPwd = Encoding.UTF8.GetString(Convert.FromBase64String(encodedPwd));

                StringBuilder sb = new StringBuilder();
                sb.Append($"Host={configuration.AppSettings.Settings["HostServer"].Value};");
                sb.Append($"Port={configuration.AppSettings.Settings["Port"].Value};");
                sb.Append($"Username={configuration.AppSettings.Settings["DBUsername"].Value};");
                sb.Append($"Password={decodedPwd};");

                return new NpgsqlConnection(sb.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine($"[EXCEPTION]: {ex.Message}");
                return new NpgsqlConnection();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=localhost;Database=xcryptor;Username=xcryptor_app_user;Password=p@55w0rd");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "English_United States.1252");

            modelBuilder.Entity<m_hash>(entity =>
            {
                entity.HasKey(e => e.hash_id)
                    .HasName("m_hash_pk");
            });

            modelBuilder.Entity<m_main>(entity =>
            {
                entity.HasOne(d => d.hash)
                    .WithMany(p => p.m_main)
                    .HasForeignKey(d => d.hash_id)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("m_main_m_hash_hash_id_fk");

                entity.HasOne(d => d.project)
                    .WithMany(p => p.m_main)
                    .HasForeignKey(d => d.project_id)
                    .HasConstraintName("m_main_m_project_project_id_fk");
            });

            modelBuilder.Entity<m_project>(entity =>
            {
                entity.HasKey(e => e.project_id)
                    .HasName("m_project_pk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
