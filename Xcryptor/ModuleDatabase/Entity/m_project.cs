﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ModuleDatabase.Entity
{
    [Table("m_project", Schema = "master")]
    [Index(nameof(project_id), Name = "m_project_project_id_uindex", IsUnique = true)]
    public partial class m_project
    {
        public m_project()
        {
            m_main = new HashSet<m_main>();
        }

        [Key]
        public int project_id { get; set; }
        [Required]
        [StringLength(128)]
        public string project_name { get; set; }
        [StringLength(512)]
        public string description { get; set; }

        [InverseProperty("project")]
        public virtual ICollection<m_main> m_main { get; set; }
    }
}
