﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ModuleDatabase.Entity
{
    [Table("m_hash", Schema = "master")]
    [Index(nameof(hash_id), Name = "m_hash_hash_id_uindex", IsUnique = true)]
    public partial class m_hash
    {
        public m_hash()
        {
            m_main = new HashSet<m_main>();
        }

        [Key]
        public int hash_id { get; set; }
        [Required]
        [StringLength(512)]
        public string hash { get; set; }

        [InverseProperty("hash")]
        public virtual ICollection<m_main> m_main { get; set; }
    }
}
