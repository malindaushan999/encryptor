﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ModuleDatabase.Entity
{
    [Table("m_main", Schema = "master")]
    [Index(nameof(id), Name = "m_main_id_uindex", IsUnique = true)]
    public partial class m_main
    {
        [Key]
        public int id { get; set; }
        public int project_id { get; set; }
        [StringLength(512)]
        public string encrypted_key { get; set; }
        public int? hash_id { get; set; }
        [StringLength(512)]
        public string description { get; set; }

        [ForeignKey(nameof(hash_id))]
        [InverseProperty(nameof(m_hash.m_main))]
        public virtual m_hash hash { get; set; }
        [ForeignKey(nameof(project_id))]
        [InverseProperty(nameof(m_project.m_main))]
        public virtual m_project project { get; set; }
    }
}
