﻿create database xcryptor
	with owner postgres;

grant connect, create, temporary on database xcryptor to xcryptor_app_user;

create table m_project
(
    project_id   serial       not null
        constraint m_project_pk
            primary key,
    project_name varchar(128) not null,
    description  varchar(512)
);

alter table m_project
    owner to postgres;

create unique index m_project_project_id_uindex
    on m_project (project_id);

create table m_hash
(
    hash_id serial       not null
        constraint m_hash_pk
            primary key,
    hash    varchar(512) not null
);

alter table m_hash
    owner to postgres;

create table m_main
(
    id            serial  not null
        constraint m_main_pk
            primary key,
    project_id    integer not null
        constraint m_main_m_project_project_id_fk
            references m_project,
    encrypted_key varchar(512),
    hash_id       integer not null
        constraint m_main_m_hash_hash_id_fk
            references m_hash
);

alter table m_main
    owner to postgres;

create unique index m_main_id_uindex
    on m_main (id);

create unique index m_hash_hash_id_uindex
    on m_hash (hash_id);

