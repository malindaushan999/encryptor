﻿using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Windows.ApplicationModel;
using Windows.Security.Credentials;
using Windows.Storage.Streams;
using Windows.System;
using Xcryptor.Common;
using Xcryptor.Views;

namespace Xcryptor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            KeyCredentialRetrievalResult authResult = null;
            Task.Run(async () => { authResult = await LoginExecute(); }).Wait();

            if (authResult == null || authResult.Status != KeyCredentialStatus.Success)
            {
                Application.Current.Shutdown();
            }

            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<ModuleDatabase.ModuleDatabaseModule>();
        }

        private async Task<KeyCredentialRetrievalResult> LoginExecute()
        {
            KeyCredentialRetrievalResult authResult = null;
            string message = "";

            try
            {
                var isWindowHelloSupported = await KeyCredentialManager.IsSupportedAsync();

                if (isWindowHelloSupported)
                {
                    authResult = await KeyCredentialManager.RequestCreateAsync("Xcryptor", KeyCredentialCreationOption.ReplaceExisting);
                    if (authResult.Status == KeyCredentialStatus.Success)
                    {
                        IReadOnlyList<User> users = await User.FindAllAsync();

                        var current = users.Where(u => u.AuthenticationStatus == UserAuthenticationStatus.LocallyAuthenticated &&
                                                       u.Type == UserType.LocalUser).FirstOrDefault();

                        // user may have username
                        var data = await current.GetPropertyAsync(KnownUserProperties.AccountName);
                        string displayName = (string)data;

                        // or may be authenticated using hotmail/outlook
                        string firstName = (string)await current.GetPropertyAsync(KnownUserProperties.FirstName);
                        string lastName = (string)await current.GetPropertyAsync(KnownUserProperties.LastName);
                        if (!string.IsNullOrEmpty(firstName) || !string.IsNullOrEmpty(lastName))
                        {
                            displayName = string.Format("{0} {1}", firstName, lastName);
                        }

                        message = $"Logged in as {displayName}";
                        Application.Current.Properties["loggedUser"] = displayName;
                    }
                    else
                    {
                        message = "Login error: " + authResult.Status;
                    }
                }
                else
                {
                    message = "Windows Hello is not enabled for this device.";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION]: ", ex.Message);
            }

#if !DEBUG
            if (!string.IsNullOrEmpty(message))
            {
                BaseHelper.ShowToast(message); 
            }
#endif
            return authResult;
        }
    }
}
