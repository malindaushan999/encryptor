﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcryptor.Common
{
    public static class StaticData
    {
        public static readonly string DarkThemeToolTip = "Light Mode";
        public static readonly string LightThemeToolTip = "Dark Mode";

        public static readonly string KeyColumnHeader_Encrypted = "Encrypted Key";
        public static readonly string KeyColumnHeader_Decrypted = "Decrypted Key";
    }
}
