﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcryptor.Common
{
    public class CodeValuePair : BindableBase
    {
        private int _code;
        public int Code
        {
            get { return _code; }
            set
            {
                SetProperty(ref _code, value);
            }
        }

        private string _value;
        public string Value
        {
            get { return _value; }
            set
            {
                SetProperty(ref _value, value);
            }
        }

        public object Extra { get; set; }
    }
}
