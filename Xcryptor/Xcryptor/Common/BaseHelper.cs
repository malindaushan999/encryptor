﻿using MaterialDesignThemes.Wpf;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Windows.UI.Notifications;
using Xcryptor.ViewModels;
using Xcryptor.Views;
using static Xcryptor.Common.CommonEnum;

namespace Xcryptor.Common
{
    public static class BaseHelper
    {
        public static void ShowToast(string message)
        {
            ToastContent content = new ToastContentBuilder()
                .AddHeader("6289","Xcryptor Login", "action=openConversation&amp;id=6289")
                .AddText(message)
                .GetToastContent();

            ToastNotification toast = new ToastNotification(content.GetXml());
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        public static async void ShowDialog(string message, string headerText = "Message", DIALOG_BOX_TYPE type = DIALOG_BOX_TYPE.WARNING)
        {
            var dataContext = new DialogBoxViewModel()
            {
                HeaderText = headerText,
                MessageText = message,
                DialogBoxType = type
            };
            var dialogContent = new DialogBox
            {
                DataContext = dataContext
            };
            await MaterialDesignThemes.Wpf.DialogHost.Show(dialogContent);
        }
    }
}
