﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xcryptor.Common
{
    public class CommonEnum
    {
        public enum TOGGLE_MODE
        {
            ENCRYPT = 0,
            DECRYPT = 1
        }

        public enum DIALOG_BOX_TYPE
        {
            INFO = 0,
            ERROR = 1,
            WARNING = 2
        }
    }
}
