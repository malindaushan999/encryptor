﻿using MaterialDesignThemes.Wpf;
using ModuleDatabase.Entity;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Xcryptor.ViewModels
{
    public class ProjectAddViewModel : BindableBase, IDataErrorInfo
    {
        #region Binding Properties
        
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _titleIcon;
        public string TitleIcon
        {
            get { return _titleIcon; }
            set { SetProperty(ref _titleIcon, value); }
        }

        private string _titleIconColor;
        public string TitleIconColor
        {
            get { return _titleIconColor; }
            set { SetProperty(ref _titleIconColor, value); }
        }

        private string _buttonText;
        public string ButtonText
        {
            get { return _buttonText; }
            set { SetProperty(ref _buttonText, value); }
        }

        private int _projectID;
        public int ProjectID
        {
            get { return _projectID; }
            set { SetProperty(ref _projectID, value); }
        }

        private string _projectName;
        public string ProjectName
        {
            get { return _projectName; }
            set { SetProperty(ref _projectName, value); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        private bool _isNewProject = false;
        public bool IsNewProject
        {
            get { return _isNewProject; }
            set
            {
                _isNewProject = value;
                if (value)
                {
                    TitleIcon = "PlusThick";
                    TitleIconColor = "Green";
                    Title = "ADD NEW PROJECT";
                    ButtonText = " ADD";
                }
                else
                {
                    TitleIcon = "FileDocumentEdit";
                    TitleIconColor = "Orange";
                    Title = $"EDIT PROJECT - ID {ProjectID}";
                    ButtonText = " EDIT";
                }
            }
        }

        public DelegateCommand SaveCommand { get; set; }

        #endregion Binding Properties

        #region IDataErrorInfo

        public string Error => string.Empty;

        public string this[string columnName]
        {
            get
            {
                string result = null;
                switch (columnName)
                {
                    case nameof(ProjectName):
                        if (string.IsNullOrEmpty(ProjectName))
                        {
                            result = "Please enter project name.";
                        }
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        #endregion IDataErrorInfo

        /// <summary>
        /// Constructor
        /// </summary>
        public ProjectAddViewModel()
        {
            SaveCommand = new DelegateCommand(SaveExecute);
        }

        #region Execute Commands

        private void SaveExecute()
        {
            if (string.IsNullOrEmpty(ProjectName))
            {
                return;
            }

            if (IsNewProject)
            {
                AddNewProject();
            }
            else
            {
                EditProject();
            }
        }

        #endregion Execute Commands

        #region Private Methods

        private void AddNewProject()
        {
            try
            {
                using (var dbContext = new XcryptorMasterContext())
                {
                    var projectEntity = dbContext.m_project.FirstOrDefault(x => x.project_name == ProjectName);
                    if (projectEntity is not null)
                    {
                        Status = $"\"{ProjectName}\" is already exists.";
                        return;
                    }

                    projectEntity = new();
                    projectEntity.project_name = ProjectName;
                    projectEntity.description = Description;

                    dbContext.m_project.Add(projectEntity);
                    dbContext.SaveChanges();

                    DialogHost.CloseDialogCommand.Execute(null, null);
                }
            }
            catch (Exception ex)
            {
                Status = $"New project add operation failed. \n[Exception]: {ex.Message}";
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
            }
        }

        private void EditProject()
        {
            try
            {
                using (var dbContext = new XcryptorMasterContext())
                {
                    var projectEntity = dbContext.m_project.FirstOrDefault(x => x.project_id == ProjectID);
                    if (projectEntity is null)
                    {
                        Status = $"Can't find the project.";
                        return;
                    }

                    projectEntity.project_name = ProjectName;
                    projectEntity.description = Description;

                    dbContext.SaveChanges();

                    DialogHost.CloseDialogCommand.Execute(null, null);
                }
            }
            catch (Exception ex)
            {
                Status = $"Project edit operation failed. \n[Exception]: {ex.Message}";
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
            }
        }

        #endregion Private Methods
    }
}
