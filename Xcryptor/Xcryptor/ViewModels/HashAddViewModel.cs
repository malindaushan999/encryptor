﻿using MaterialDesignThemes.Wpf;
using ModuleDatabase.Entity;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Xcryptor.ViewModels
{
    public class HashAddViewModel : BindableBase, IDataErrorInfo
    {
        #region Binding Properties

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _titleIcon;
        public string TitleIcon
        {
            get { return _titleIcon; }
            set { SetProperty(ref _titleIcon, value); }
        }

        private string _titleIconColor;
        public string TitleIconColor
        {
            get { return _titleIconColor; }
            set { SetProperty(ref _titleIconColor, value); }
        }

        private string _buttonText;
        public string ButtonText
        {
            get { return _buttonText; }
            set { SetProperty(ref _buttonText, value); }
        }

        private int _hashID;
        public int HashID
        {
            get { return _hashID; }
            set { SetProperty(ref _hashID, value); }
        }

        private string _hashValue;
        public string HashValue
        {
            get { return _hashValue; }
            set { SetProperty(ref _hashValue, value); }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        private bool _isNewHashValue = false;
        public bool IsNewHashValue
        {
            get { return _isNewHashValue; }
            set
            {
                _isNewHashValue = value;
                if (value)
                {
                    TitleIcon = "PlusThick";
                    TitleIconColor = "Green";
                    Title = "ADD NEW HASH VALUE";
                    ButtonText = " ADD";
                }
                else
                {
                    TitleIcon = "FileDocumentEdit";
                    TitleIconColor = "Orange";
                    Title = $"EDIT HASH VALUE - ID {HashID}";
                    ButtonText = " EDIT";
                }
            }
        }

        public DelegateCommand SaveCommand { get; set; }

        #endregion Binding Properties

        #region IDataErrorInfo

        public string Error => string.Empty;

        public string this[string columnName]
        {
            get
            {
                string result = null;
                switch (columnName)
                {
                    case nameof(HashValue):
                        if (string.IsNullOrEmpty(HashValue))
                        {
                            result = "Please enter hash value.";
                        }
                        break;
                    default:
                        break;
                }
                return result;
            }
        }

        #endregion IDataErrorInfo

        /// <summary>
        /// Constructor
        /// </summary>
        public HashAddViewModel()
        {
            SaveCommand = new DelegateCommand(SaveExecute);
        }

        #region Execute Commands

        private void SaveExecute()
        {
            if (string.IsNullOrEmpty(HashValue))
            {
                return;
            }

            if (IsNewHashValue)
            {
                AddNewHashValue();
            }
            else
            {
                EditHashValue();
            }
        }

        #endregion Execute Commands

        #region Private Methods

        private void AddNewHashValue()
        {
            try
            {
                using (var dbContext = new XcryptorMasterContext())
                {
                    var hashEntity = dbContext.m_hash.FirstOrDefault(x => x.hash == HashValue);
                    if (hashEntity is not null)
                    {
                        Status = $"\"{HashValue}\" is already exists.";
                        return;
                    }

                    hashEntity = new();
                    hashEntity.hash = HashValue;

                    dbContext.m_hash.Add(hashEntity);
                    dbContext.SaveChanges();

                    DialogHost.CloseDialogCommand.Execute(null, null);
                }
            }
            catch (Exception ex)
            {
                Status = $"New hash value add operation failed. \n[Exception]: {ex.Message}";
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
            }
        }

        private void EditHashValue()
        {
            try
            {
                using (var dbContext = new XcryptorMasterContext())
                {
                    var projectEntity = dbContext.m_hash.FirstOrDefault(x => x.hash_id == HashID);
                    if (projectEntity is null)
                    {
                        Status = $"Can't find the hash value.";
                        return;
                    }

                    projectEntity.hash = HashValue;

                    dbContext.SaveChanges();

                    DialogHost.CloseDialogCommand.Execute(null, null);
                }
            }
            catch (Exception ex)
            {
                Status = $"Hash value edit operation failed. \n[Exception]: {ex.Message}";
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
            }
        }

        #endregion Private Methods
    }
}
