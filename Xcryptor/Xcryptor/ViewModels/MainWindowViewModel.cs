﻿using MaterialDesignThemes.Wpf;
using ModuleDatabase.Entity;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Media;
using Windows.UI.ViewManagement;
using Xcryptor.Common;
using Xcryptor.Views;
using static Xcryptor.Common.CommonEnum;

namespace Xcryptor.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly PaletteHelper _paletteHelper = new PaletteHelper();
        private static TOGGLE_MODE _toggleMode = TOGGLE_MODE.ENCRYPT;

        #region Binding Properties

        private string _title = "Xcrypter App";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _loggedUser;
        public string LoggedUser
        {
            get { return _loggedUser; }
            set { SetProperty(ref _loggedUser, value); }
        }

        private string _themeModeToolTip = StaticData.LightThemeToolTip;
        public string ThemeModeToolTip
        {
            get { return _themeModeToolTip; }
            set { SetProperty(ref _themeModeToolTip, value); }
        }

        private bool _isDarkTheme;
        public bool IsDarkTheme
        {
            get => _isDarkTheme;
            set
            {
                if (SetProperty(ref _isDarkTheme, value))
                {
                    ThemeModeToolTip = value ? StaticData.DarkThemeToolTip : StaticData.LightThemeToolTip;
                    ApplyBase(value);
                }
            }
        }

        #endregion Binding Properties

        #region Binding Collections

        private ObservableCollection<KeyInfoDataGrid> _keyInfoDataGrid = new();
        public ObservableCollection<KeyInfoDataGrid> KeyInfoDataGridList
        {
            get { return _keyInfoDataGrid; }
            set { SetProperty(ref _keyInfoDataGrid, value); }
        }

        private ObservableCollection<CodeValuePair> _projectList = new();
        public ObservableCollection<CodeValuePair> ProjectList
        {
            get { return _projectList; }
            set { SetProperty(ref _projectList, value); }
        }

        private ObservableCollection<CodeValuePair> _hashList = new();
        public ObservableCollection<CodeValuePair> HashList
        {
            get { return _hashList; }
            set { SetProperty(ref _hashList, value); }
        }

        #endregion Binding Collections

        #region Binding Commands

        public DelegateCommand<string> EncryptDecryptToggleCommand { get; set; }

        public DelegateCommand AddNewRecordCommand { get; set; }
        public DelegateCommand<int?> SaveRecordCommand { get; set; }
        public DelegateCommand<int?> DeleteRecordCommand { get; set; }
        public DelegateCommand SaveAllCommand { get; set; }

        public DelegateCommand<object> EncryptCommand { get; set; }
        public DelegateCommand<object> DecryptCommand { get; set; }

        public DelegateCommand<int?> AddNewProjectCommand { get; set; }
        public DelegateCommand<object> EditProjectCommand { get; set; }
        public DelegateCommand<int?> DeleteProjectCommand { get; set; }

        public DelegateCommand<int?> AddNewHashCommand { get; set; }
        public DelegateCommand<object> EditHashCommand { get; set; }
        public DelegateCommand<int?> DeleteHashCommand { get; set; }

        #endregion Binding Commands

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindowViewModel()
        {
            Init();
        }

        #region Initializations

        private void Init()
        {
            SetInitialTheme();
            LoggedUser = Application.Current.Properties["loggedUser"].ToString();

            EncryptDecryptToggleCommand = new DelegateCommand<string>(EncryptDecryptToggleExecute);

            AddNewRecordCommand = new DelegateCommand(AddNewRecordExecute);
            SaveRecordCommand = new DelegateCommand<int?>(SaveRecordExecute);
            DeleteRecordCommand = new DelegateCommand<int?>(DeleteRecordExecute);
            SaveAllCommand = new DelegateCommand(SaveAllExecute);

            EncryptCommand = new DelegateCommand<object>(EncryptExecute);
            DecryptCommand = new DelegateCommand<object>(DecryptExecute);

            AddNewProjectCommand = new DelegateCommand<int?>(AddNewProjectExecute);
            EditProjectCommand = new DelegateCommand<object>(EditProjectExecute);
            DeleteProjectCommand = new DelegateCommand<int?>(DeleteProjectExecute);

            AddNewHashCommand = new DelegateCommand<int?>(AddNewHashExecute);
            EditHashCommand = new DelegateCommand<object>(EditHashExecute);
            DeleteHashCommand = new DelegateCommand<int?>(DeleteHashExecute);

            LoadDropdownData();
            LoadData();
        }

        private void ApplyBase(bool isDark)
        {
            ITheme theme = _paletteHelper.GetTheme();
            IBaseTheme baseTheme = isDark ? new MaterialDesignDarkTheme() : (IBaseTheme)new MaterialDesignLightTheme();
            theme.SetBaseTheme(baseTheme);
            _paletteHelper.SetTheme(theme);
        }

        private void SetInitialTheme()
        {
            var uiSettings = new UISettings();
            var color = uiSettings.GetColorValue(UIColorType.Background);
            if (color.ToString() == Colors.Black.ToString())
            {
                IsDarkTheme = true;
            }
        }

        #endregion Initializations

        #region Dropdown Data Operations

        private void LoadDropdownData()
        {
            GetProjectList();
            GetHashList();
        }

        private void GetProjectList()
        {
            try
            {
                List<CodeValuePair> cbList = new();
                using (var dbContext = new XcryptorMasterContext())
                {
                    var dataList = dbContext.m_project.ToList();
                    foreach (var item in dataList)
                    {
                        CodeValuePair cbItem = new()
                        {
                            Code = item.project_id,
                            Value = item.project_name,
                            Extra = item
                        };

                        cbList.Add(cbItem);
                    }
                }

                ProjectList = new ObservableCollection<CodeValuePair>(cbList);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while retrieving project list.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private void GetHashList()
        {
            try
            {
                List<CodeValuePair> cbList = new();
                using (var dbContext = new XcryptorMasterContext())
                {
                    var dataList = dbContext.m_hash.ToList();
                    foreach (var item in dataList)
                    {
                        CodeValuePair cbItem = new()
                        {
                            Code = item.hash_id,
                            Value = item.hash,
                            Extra = item
                        };

                        cbList.Add(cbItem);
                    }
                }

                HashList = new ObservableCollection<CodeValuePair>(cbList);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while retrieving hash value list.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        #endregion Dropdown Data Operations

        #region Data Operations

        private void LoadData()
        {
            try
            {
                KeyInfoDataGridList.Clear();

                using (var dbContext = new XcryptorMasterContext())
                {
                    var dataList = dbContext.m_main.ToList();
                    foreach (var item in dataList)
                    {
                        KeyInfoDataGrid gridDataRow = new()
                        {
                            IDX = item.id,
                            SelectedProject = ProjectList.FirstOrDefault(x => x.Code == item.project_id),
                            Description = item.description,
                            EncryptedKey = item.encrypted_key,
                            SelectedHash = HashList.FirstOrDefault(x => x.Code == item.hash_id)
                        };

                        try
                        {
                            gridDataRow.DecryptedKey = Cipher.DecryptString(item.encrypted_key, gridDataRow.SelectedHash?.Value);
                        }
                        catch (Exception ex)
                        {
                            string message = string.Format("{0} {1}", "Cannot decrypt the input data.", "Hash key has been changed.");
                            Console.WriteLine(message);
                        }

                        KeyInfoDataGridList.Add(gridDataRow);
                    }
                }

                KeyInfoDataGridList.ToList().ForEach(x => x.IsEdited = false);
            }
            catch (Exception ex)
            {
                string message = string.Format("{0}\n\n[Exception]: {1}",
                    "Exception occurred.",
                    ex.Message);
                BaseHelper.ShowDialog(message, "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        #endregion Data Operations

        #region Command Execute Operations

        #region Hash Value CRUD Operations

        private void DeleteHashExecute(int? hashID)
        {
            try
            {
                using (var dbContext = new XcryptorMasterContext())
                {
                    var hashEntity = dbContext.m_hash.FirstOrDefault(x => x.hash_id == hashID);
                    if (hashEntity == null)
                    {
                        BaseHelper.ShowDialog($"Can't find the hash value.", "Error", DIALOG_BOX_TYPE.ERROR);
                    }

                    dbContext.m_hash.Remove(hashEntity);
                    dbContext.SaveChanges();

                    HashList.RemoveAll(x => x.Code == hashID);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while trying to delete hash value.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private async void EditHashExecute(object extra)
        {
            try
            {
                m_hash hash = (m_hash)extra;
                var dataContext = new HashAddViewModel()
                {
                    HashID = hash.hash_id,
                    HashValue = hash.hash,
                    IsNewHashValue = false
                };
                var dialogContent = new HashAdd
                {
                    DataContext = dataContext
                };
                await DialogHost.Show(dialogContent);

                using (var dbContext = new XcryptorMasterContext())
                {
                    var entity = dbContext.m_hash.FirstOrDefault(x => x.hash_id == hash.hash_id);

                    var selectedHash = HashList.FirstOrDefault(x => x.Code == hash.hash_id);
                    if (entity != null && selectedHash != null)
                    {
                        selectedHash.Value = entity.hash;
                        selectedHash.Extra = entity;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while trying to edit hash value.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private async void AddNewHashExecute(int? idx)
        {
            try
            {
                var dataContext = new HashAddViewModel()
                {
                    IsNewHashValue = true
                };
                var dialogContent = new HashAdd
                {
                    DataContext = dataContext
                };
                await DialogHost.Show(dialogContent);

                using (var dbContext = new XcryptorMasterContext())
                {
                    var entity = dbContext.m_hash.OrderByDescending(x => x.hash_id).FirstOrDefault();
                    if (entity != null && HashList.FirstOrDefault(x => x.Code == entity.hash_id) == null)
                    {
                        var cvPair = new CodeValuePair()
                        {
                            Code = entity.hash_id,
                            Value = entity.hash,
                            Extra = entity
                        };

                        HashList.Add(cvPair);

                        var rowData = KeyInfoDataGridList.FirstOrDefault(x => x.IDX == idx);
                        if (rowData != null)
                        {
                            rowData.SelectedHash = cvPair;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while trying to add hash value.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        #endregion Hash Value CRUD Operations

        #region Project CRUD Operations

        private void DeleteProjectExecute(int? projectID)
        {
            try
            {
                using (var dbContext = new XcryptorMasterContext())
                {
                    var projectEntity = dbContext.m_project.FirstOrDefault(x => x.project_id == projectID);
                    if (projectEntity == null)
                    {
                        BaseHelper.ShowDialog($"Can't find the project.", "Error", DIALOG_BOX_TYPE.ERROR);
                        return;
                    }

                    dbContext.m_project.Remove(projectEntity);
                    dbContext.SaveChanges();

                    KeyInfoDataGridList.RemoveAll(x => !x.IsEdited && x.SelectedProject != null && x.SelectedProject.Code == projectID);
                    ProjectList.RemoveAll(x => x.Code == projectID);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while trying to delete project.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private async void EditProjectExecute(object extra)
        {
            try
            {
                m_project project = (m_project)extra;
                var dataContext = new ProjectAddViewModel()
                {
                    ProjectID = project.project_id,
                    ProjectName = project.project_name,
                    Description = project.description,
                    IsNewProject = false
                };
                var dialogContent = new ProjectAdd
                {
                    DataContext = dataContext
                };
                await DialogHost.Show(dialogContent);

                using (var dbContext = new XcryptorMasterContext())
                {
                    var entity = dbContext.m_project.FirstOrDefault(x => x.project_id == project.project_id);

                    var selectedProj = ProjectList.FirstOrDefault(x => x.Code == project.project_id);
                    if (entity != null && selectedProj != null)
                    {
                        selectedProj.Value = entity.project_name;
                        selectedProj.Extra = entity;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while trying to edit project.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private async void AddNewProjectExecute(int? idx)
        {
            try
            {
                var dataContext = new ProjectAddViewModel()
                {
                    IsNewProject = true
                };
                var dialogContent = new ProjectAdd
                {
                    DataContext = dataContext
                };
                await DialogHost.Show(dialogContent);

                using (var dbContext = new XcryptorMasterContext())
                {
                    var entity = dbContext.m_project.OrderByDescending(x => x.project_id).FirstOrDefault();
                    if (entity != null && ProjectList.FirstOrDefault(x => x.Code == entity.project_id) == null)
                    {
                        var cvPair = new CodeValuePair()
                        {
                            Code = entity.project_id,
                            Value = entity.project_name,
                            Extra = entity
                        };
                        ProjectList.Add(cvPair);

                        var rowData = KeyInfoDataGridList.FirstOrDefault(x => x.IDX == idx);
                        if (rowData != null)
                        {
                            rowData.SelectedProject = cvPair;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => ", ex.Message);
                BaseHelper.ShowDialog($"Error occurred while trying to add project.", "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        #endregion Project CRUD Operations

        #region Encrypt Decrypt Operations

        private void EncryptExecute(object param)
        {
            object[] values = (object[])param;
            int idx = (int)values[0];
            string decryptedKey = values[1]?.ToString();
            string hash = ((CodeValuePair)values[2])?.Value;

            if (string.IsNullOrEmpty(decryptedKey))
            {
                BaseHelper.ShowDialog("Decrypted key is empty.");
                return;
            }
            if (string.IsNullOrEmpty(hash))
            {
                BaseHelper.ShowDialog("Hash value is empty.");
                return;
            }

            string encryptedKey = Cipher.EncryptString(decryptedKey, hash);

            var gridData = KeyInfoDataGridList.FirstOrDefault(x => x.IDX == idx);
            if (gridData is not null)
            {
                gridData.EncryptedKey = encryptedKey;
            }
        }

        private void DecryptExecute(object param)
        {
            try
            {
                object[] values = (object[])param;
                int idx = (int)values[0];
                string encryptedKey = values[1].ToString();
                string hash = ((CodeValuePair)values[2])?.Value;

                if (string.IsNullOrEmpty(encryptedKey))
                {
                    BaseHelper.ShowDialog("Encrypted key is empty.");
                    return;
                }
                if (string.IsNullOrEmpty(hash))
                {
                    BaseHelper.ShowDialog("Hash value is empty.");
                    return;
                }

                string decryptedKey = Cipher.DecryptString(encryptedKey, hash);

                var gridData = KeyInfoDataGridList.FirstOrDefault(x => x.IDX == idx);
                if (gridData is not null)
                {
                    gridData.DecryptedKey = decryptedKey;
                }
            }
            catch (FormatException ex)
            {
                string message = string.Format("{0} {1}", "Cannot decrypt the input data.", "Invalid encrypted key.");
                BaseHelper.ShowDialog(message, "Error", DIALOG_BOX_TYPE.ERROR);
            }
            catch (CryptographicException ex)
            {
                string message = string.Format("{0} {1}", "Cannot decrypt the input data.", "Hash key has been changed.");
                BaseHelper.ShowDialog(message, "Error", DIALOG_BOX_TYPE.ERROR);
            }
            catch (Exception ex)
            {
                string message = string.Format("{0}\n\n[Exception]: {1}",
                    "Cannot decrypt the input data.",
                    ex.Message);
                BaseHelper.ShowDialog(message, "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        #endregion Encrypt Decrypt Operations

        private string ValidationForSave(int? idx)
        {
            KeyInfoDataGrid record = KeyInfoDataGridList.FirstOrDefault(x => x.IDX == idx);

            if (record == null)
            {
                return "ID is not found.";
            }
            if (record.SelectedProject == null || record.SelectedProject.Code < 1)
            {
                return "Please select a project.";
            }
            if (string.IsNullOrEmpty(record.EncryptedKey))
            {
                return "Please encrypt the key.";
            }
            if (record.SelectedHash == null || record.SelectedHash.Code < 1)
            {
                return "Please select a hash key.";
            }

            return null;
        }

        private string ValidationForSave()
        {
            if (KeyInfoDataGridList.Where(x => x.SelectedProject == null || x.SelectedProject.Code < 1).Any())
            {
                return "Please select a project(s).";
            }
            if (KeyInfoDataGridList.Where(x => string.IsNullOrEmpty(x.EncryptedKey)).Any())
            {
                return "Please encrypt the key(s).";
            }
            if (KeyInfoDataGridList.Where(x => x.SelectedHash == null || x.SelectedHash.Code < 1).Any())
            {
                return "Please select a hash key(s).";
            }

            return null;
        }

        private void SaveRecordExecute(int? idx)
        {
            try
            {
                string validationMessage = ValidationForSave(idx);
                if (!string.IsNullOrEmpty(validationMessage))
                {
                    BaseHelper.ShowDialog(validationMessage, "Validation Failed", DIALOG_BOX_TYPE.WARNING);
                    return;
                }

                using (var dbContext = new XcryptorMasterContext())
                {
                    var dataRow = KeyInfoDataGridList.FirstOrDefault(x => x.IDX == idx);

                    var entity = dbContext.m_main.FirstOrDefault(x => x.id == idx);
                    if (entity == null)
                    {
                        // Add New record to db
                        entity = new()
                        {
                            project_id = dataRow.SelectedProject.Code,
                            description = dataRow.Description,
                            encrypted_key = dataRow.EncryptedKey,
                            hash_id = dataRow.SelectedHash.Code
                        };
                        dbContext.m_main.Add(entity);
                    }
                    else
                    {
                        // Edit record in db
                        entity.project_id = dataRow.SelectedProject.Code;
                        entity.description = dataRow.Description;
                        entity.encrypted_key = dataRow.EncryptedKey;
                        entity.hash_id = dataRow.SelectedHash.Code;
                    }

                    dbContext.SaveChanges();

                    dataRow.IDX = entity.id;
                    dataRow.IsNewRecord = false;
                    dataRow.IsEdited = false;

                    BaseHelper.ShowDialog("Record saved successfully.", "Info", DIALOG_BOX_TYPE.INFO);
                }
            }
            catch (CryptographicException ex)
            {
                string message = string.Format("{0} {1}", "Cannot decrypt the input data.", "Hash key has been changed.");
                BaseHelper.ShowDialog(message, "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private void SaveAllExecute()
        {
            try
            {
                string validationMessage = ValidationForSave();
                if (!string.IsNullOrEmpty(validationMessage))
                {
                    BaseHelper.ShowDialog(validationMessage, "Validation Failed", DIALOG_BOX_TYPE.WARNING);
                    return;
                }

                if (!KeyInfoDataGridList.Where(x => x.IsEdited || x.IsNewRecord).Any())
                {
                    BaseHelper.ShowDialog("No new or edited records found.", "Warning", DIALOG_BOX_TYPE.WARNING);
                    return;
                }

                using (var dbContext = new XcryptorMasterContext())
                {
                    foreach (var dataRow in KeyInfoDataGridList.Where(x => x.IsEdited || x.IsNewRecord))
                    {
                        var entity = dbContext.m_main.FirstOrDefault(x => x.id == dataRow.IDX);
                        if (entity == null)
                        {
                            // Add New record to db
                            entity = new()
                            {
                                project_id = dataRow.SelectedProject.Code,
                                description = dataRow.Description,
                                encrypted_key = dataRow.EncryptedKey,
                                hash_id = dataRow.SelectedHash.Code
                            };
                            dbContext.m_main.Add(entity);
                        }
                        else
                        {
                            // Edit record in db
                            entity.project_id = dataRow.SelectedProject.Code;
                            entity.description = dataRow.Description;
                            entity.encrypted_key = dataRow.EncryptedKey;
                            entity.hash_id = dataRow.SelectedHash.Code;
                        }
                    }

                    dbContext.SaveChanges();

                    LoadData();

                    BaseHelper.ShowDialog("All records saved successfully.", "Info", DIALOG_BOX_TYPE.INFO);
                }
            }
            catch (CryptographicException ex)
            {
                string message = string.Format("{0} {1}", "Cannot decrypt the input data.", "Hash key has been changed.");
                BaseHelper.ShowDialog(message, "Error", DIALOG_BOX_TYPE.ERROR);
            }
        }

        private void DeleteRecordExecute(int? idx)
        {
            if (idx is not null && idx > 0)
            {
                try
                {
                    using (var dbContext = new XcryptorMasterContext())
                    {
                        var entity = dbContext.m_main.FirstOrDefault(x => x.id == idx);
                        if (entity == null)
                        {
                            KeyInfoDataGridList.Remove(KeyInfoDataGridList.SingleOrDefault(x => x.IDX == idx));
                            KeyInfoDataGridList.Select((x, v) => x.IDX = v + 1);
                            return;
                        }

                        dbContext.m_main.Remove(entity);
                        dbContext.SaveChanges();
                    }

                    KeyInfoDataGridList.Remove(KeyInfoDataGridList.SingleOrDefault(x => x.IDX == idx));
                    KeyInfoDataGridList.Select((x, v) => x.IDX = v + 1);

                    BaseHelper.ShowDialog("Record deleted successfully.", "Info", DIALOG_BOX_TYPE.INFO);
                }
                catch (Exception ex)
                {
                    BaseHelper.ShowDialog("Error occurred while deleting the data record.", "Error", DIALOG_BOX_TYPE.ERROR);
                    Console.WriteLine("[EXCEPTION] => ", ex.Message);
                }
            }
        }

        private void AddNewRecordExecute()
        {
            var rowData = new KeyInfoDataGrid()
            {
                IsNewRecord = true,
                IDX = KeyInfoDataGridList.Count > 0 ? KeyInfoDataGridList.Max(x => x.IDX) + 1 : 1,
                RowBorderColor = Colors.Green.ToString()
            };

            switch (_toggleMode)
            {
                case TOGGLE_MODE.ENCRYPT:
                    rowData.IsEnabledDecryptedKey = true;
                    rowData.IsEnabledEncryptedKey = false;
                    break;
                case TOGGLE_MODE.DECRYPT:
                    rowData.IsEnabledEncryptedKey = true;
                    rowData.IsEnabledDecryptedKey = false;
                    break;
                default:
                    break;
            }

            KeyInfoDataGridList.Add(rowData);
            KeyInfoDataGridList.Select((x, v) => x.IDX = v + 1);
        }

        private void EncryptDecryptToggleExecute(string mode)
        {
            if (!string.IsNullOrEmpty(mode) && short.TryParse(mode, out short modeValue))
            {
                switch (modeValue)
                {
                    case (short)TOGGLE_MODE.ENCRYPT:
                        KeyInfoDataGridList.ToList().ForEach(x => x.IsEnabledDecryptedKey = true);
                        KeyInfoDataGridList.ToList().ForEach(x => x.IsEnabledEncryptedKey = false);
                        _toggleMode = TOGGLE_MODE.ENCRYPT;
                        break;
                    case (short)TOGGLE_MODE.DECRYPT:
                        KeyInfoDataGridList.ToList().ForEach(x => x.IsEnabledEncryptedKey = true);
                        KeyInfoDataGridList.ToList().ForEach(x => x.IsEnabledDecryptedKey = false);
                        _toggleMode = TOGGLE_MODE.DECRYPT;
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion Command Execute Operations

    }

    /// <summary>
    /// Key Info Data Grid
    /// </summary>
    public class KeyInfoDataGrid : BindableBase, IDataErrorInfo
    {
        #region Binding Properties

        private int _idx;
        public int IDX
        {
            get { return _idx; }
            set { SetProperty(ref _idx, value); }
        }

        private CodeValuePair _selectedProject;
        public CodeValuePair SelectedProject
        {
            get { return _selectedProject; }
            set
            {
                if (SetProperty(ref _selectedProject, value))
                {
                    IsEdited = true;
                }
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                if (SetProperty(ref _description, value))
                {
                    IsEdited = true;
                }
            }
        }

        private string _encryptedKey;
        public string EncryptedKey
        {
            get { return _encryptedKey; }
            set
            {
                if (SetProperty(ref _encryptedKey, value))
                {
                    IsEdited = true;
                }
            }
        }

        private string _decryptedKey;
        public string DecryptedKey
        {
            get { return _decryptedKey; }
            set
            {
                if (SetProperty(ref _decryptedKey, value))
                {
                    IsEdited = true;
                }
            }
        }

        private CodeValuePair _selectedHash;
        public CodeValuePair SelectedHash
        {
            get { return _selectedHash; }
            set
            {
                if (SetProperty(ref _selectedHash, value))
                {
                    IsEdited = true;
                }
            }
        }

        private bool _isEnabledEncryptedKey = false;
        public bool IsEnabledEncryptedKey
        {
            get { return _isEnabledEncryptedKey; }
            set { SetProperty(ref _isEnabledEncryptedKey, value); }
        }

        private bool _isEnabledDecryptedKey = true;
        public bool IsEnabledDecryptedKey
        {
            get { return _isEnabledDecryptedKey; }
            set { SetProperty(ref _isEnabledDecryptedKey, value); }
        }

        private bool _isEdited;
        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                if (SetProperty(ref _isEdited, value))
                {
                    if (!IsNewRecord)
                    {
                        if (value)
                        {
                            RowBorderColor = Colors.Orange.ToString();
                        }
                        else
                        {
                            RowBorderColor = Colors.Gray.ToString();
                        }
                    }
                }
            }
        }

        private bool _isNewRecord;
        public bool IsNewRecord
        {
            get { return _isNewRecord; }
            set
            {
                SetProperty(ref _isNewRecord, value);
            }
        }

        private string _rowBorderColor = Colors.Gray.ToString();
        public string RowBorderColor
        {
            get { return _rowBorderColor; }
            set { SetProperty(ref _rowBorderColor, value); }
        }

        #endregion Binding Properties

        #region IDataErrorInfo

        public string Error => string.Empty;

        public string this[string columnName]
        {
            get
            {
                string result = null;

                switch (columnName)
                {
                    case nameof(SelectedProject):
                        if (SelectedProject == null || SelectedProject.Code < 1)
                        {
                            result = "Please select a project.";
                        }
                        break;
                    case nameof(EncryptedKey):
                        if (string.IsNullOrEmpty(EncryptedKey))
                        {
                            result = "Encrypted key is empty.";
                        }
                        break;
                    case nameof(DecryptedKey):
                        if (string.IsNullOrEmpty(DecryptedKey))
                        {
                            result = "Decrypted key is empty.";
                        }
                        break;
                    case nameof(SelectedHash):
                        if (SelectedHash == null || SelectedHash.Code < 1)
                        {
                            result = "Select a Hash value.";
                        }
                        break;
                    default:
                        break;
                }

                return result;
            }
        }

        #endregion IDataErrorInfo
    }
}
