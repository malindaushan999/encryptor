﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using static Xcryptor.Common.CommonEnum;

namespace Xcryptor.ViewModels
{
    public class DialogBoxViewModel : BindableBase
    {
        private string _headerText;
        public string HeaderText
        {
            get { return _headerText; }
            set { SetProperty(ref _headerText, value); }
        }

        private string _messageText;
        public string MessageText
        {
            get { return _messageText; }
            set { SetProperty(ref _messageText, value); }
        }

        private DIALOG_BOX_TYPE _dialogBoxType;
        public DIALOG_BOX_TYPE DialogBoxType
        {
            get { return _dialogBoxType; }
            set
            {
                SetProperty(ref _dialogBoxType, value);
                switch (value)
                {
                    case DIALOG_BOX_TYPE.INFO:
                        IconKind = "Information";
                        IconColor = "SkyBlue";
                        break;
                    case DIALOG_BOX_TYPE.ERROR:
                        IconKind = "AlertOctagon";
                        IconColor = "Orange";
                        break;
                    case DIALOG_BOX_TYPE.WARNING:
                        IconKind = "Alert";
                        IconColor = "Gold";
                        break;
                    default:
                        IconKind = "Alert";
                        IconColor = "Gold";
                        break;
                }
            }
        }

        private string _iconKind;
        public string IconKind
        {
            get { return _iconKind; }
            set { SetProperty(ref _iconKind, value); }
        }

        private string _iconColor;
        public string IconColor
        {
            get { return _iconColor; }
            set { SetProperty(ref _iconColor, value); }
        }


        public DialogBoxViewModel()
        {

        }
    }
}
